from itertools import count
from pprint import pprint

from config import COOKIES, PAGE_SIZE, MARKET_ID
import requests

REWE_BASE_URL = "https://shop.rewe.de/"

default_params = {
    'objectsPerPage': str(PAGE_SIZE),
    'page': '1',
    'search': '*',
    'sorting': 'RELEVANCE_DESC',
    'serviceTypes': 'DELIVERY',
    'market': str(MARKET_ID),
    'debug': 'false',
    'autocorrect': 'true',
}

def pages():
    for page_number in count(1):
        params = {**default_params, 'page': str(page_number)}
        response = requests.get('https://shop.rewe.de/api/products', params=params, cookies=COOKIES).json()

        yield response['_embedded']['products']

        if page_number >= response['pagination']['totalPages']:
            break

for page in pages():
    for product in page:
        # TODO: have a look at the data structure. what is product['_embedded']['articles'] about?
        print(product['id'], product['productName'])
