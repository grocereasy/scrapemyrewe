[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Usage

You need to have [installed poetry](https://python-poetry.org/docs/#installing-with-pipx). Call
```bash
poetry install
```
once, then you can run the script with
```bash
poetry run python scrapemyrewe/main.py
```
from this directory.